<?php
/*
Plugin Name: Etlap PDF Fájlok Listázó
Description: Egy egyszerű plugin az etlap PDF fájlok listázásához egy adott mappából.
Version: 1.1
Author: Peter Richard - Whriley
Changelog:
Most már csak a "etlap-YYYYmmDD-XXXX.pdf" nevu pdf fajlokat hasznalja (hosszak fontosak, X->barmilyen karakter)
*/


function etlap_pdf_fajlok_shortcode($atts) {
    try {
        // Alapértelmezett attribútumok beállítása
        $atts = shortcode_atts(
            array(
                'mappa' => 'etlap_pdf',
                'db'    => 3,
            ),
            $atts,
            'etlap_pdf_fajlok'
        );

        // Mappa elérési útvonala
        $mappa_utvonal = ABSPATH . $atts['mappa'];

        // PDF fájlok listázása
        $pdf_fajlok = glob($mappa_utvonal . '/*.pdf');

        // Ha nincs találat, üres visszatérés
        if (empty($pdf_fajlok)) {
            return 'Nincs elérhető PDF fájl.';
        }

        // Fájlok rendezése a dátum alapján (legrégebbi elől)
        usort($pdf_fajlok, function ($a, $b) {
            $a_date = get_pdf_date($a);
            $b_date = get_pdf_date($b);
            return strtotime($a_date) - strtotime($b_date);
        });

        // Csak a legutolsó három dátummal rendelkező fájlok kiválasztása
        $pdf_fajlok = array_slice($pdf_fajlok, -3);

        // Sorrend visszafelé rendezése (balról növekvő sorrendben)
        // $pdf_fajlok = array_reverse($pdf_fajlok);
		
        // OUTPUT DATA FORMAT AND CALC
        // Táblázat létrehozása
        $output = '<table>';

        // Fájlok beillesztése a postba
        foreach ($pdf_fajlok as $pdf) {
            try {
                $pdf_nev = basename($pdf);
                $pdf_link = site_url('/etlap_pdf/') . $pdf_nev;

                // Dátum kinyerése a fájlnévből
                $pdf_date = get_pdf_date($pdf_nev);				
				$pdf_start_date = date('Y.m.d', strtotime($pdf_date));
                
				// Hét szombatjának kiszámítása
                $week_end = date('Y.m.d', strtotime($pdf_date . " + 5 days")); // DateConvert and DisplayFormat
				
                // Thumbnail generálása az első oldalról
                $thumbnail_url = generate_pdf_thumbnail($pdf, $atts['mappa']);

				$output .= '<td style="text-align: center; vertical-align: middle;">';
                $output .= '<a href="' . esc_url($pdf_link) . '">';
                $output .= esc_html($pdf_start_date) . '-' . esc_html($week_end);                
				$output .= '<p></p>';
				$output .= '<img src="' . esc_url($thumbnail_url) . '" alt="' . esc_attr($pdf_nev) . '">';
                $output .= '</a>';
                $output .= '</td>';
            } catch (Exception $e) {
                error_log('Error processing PDF file: ' . $pdf_nev . ' - ' . $e->getMessage());
            }
        }

        $output .= '</table>';

        return $output;
    } catch (Exception $e) {
        error_log('Error in etlap_pdf_fajlok_shortcode: ' . $e->getMessage());
        return 'Hiba a bővítmény végrehajtásakor.';
    }
}


function get_pdf_date($pdf_filename) {
    // Például a fájlnév: 20240122.pdf
	if (substr($pdf_filename, 6, -5)) {
		
		$date_part = substr($pdf_filename, 6, -9);
		// $newformat = $date_part;
		// $formatted_date = sprintf('%s.%s.%s', $year, $month, $day);
		// $formatted_date = strtotime($day+'/'+$month+'/'+$year);
		
		//$time = strtotime('10/16/2003');
		
		
		// $newformat = date('Y/m/d','10/16/2024');
		return $date_part;
	} else {
		throw new Exception('Hibás dátumformátum a fájlnévben: ' . $pdf_filename);
	}
}


function generate_pdf_thumbnail($pdf_path, $mappa) {
    $pdf_url = site_url('/' . $mappa . '/') . basename($pdf_path);
    $thumbnail_dir = ABSPATH . $mappa . '/thumbnails/';
    $thumbnail_path = $thumbnail_dir . md5($pdf_path) . '.png';

    // Ellenőrizzük, hogy a thumbnails mappa létezik-e
    if (!file_exists($thumbnail_dir)) {
        // Ha nem létezik, létrehozzuk
        mkdir($thumbnail_dir, 0755, true);
    }

    if (!file_exists($thumbnail_path)) {
        // Thumbnail létrehozása az első oldalról
        exec("convert '{$pdf_path}[0]' -thumbnail 160x -flatten '{$thumbnail_path}'");
    }

    return site_url('/' . $mappa . '/thumbnails/' . md5($pdf_path) . '.png');
}

add_shortcode('etlap_pdf_fajlok', 'etlap_pdf_fajlok_shortcode');
?>
