# Etlap PDF Fájlok Listázó

## Leírás

Ez a projekt egy egyszerű WordPress plugin, amely lehetővé teszi az étlap PDF fájlok listázását egy adott mappából és megjelenítésüket a WordPress oldalakon. A plugin automatikusan generálja a PDF fájlokhoz tartozó kisképeket és megjeleníti azokat a listázásban. Csak azok a fájlokat figyeli ami az alábbi formának megfelel: "etlap-YYYYmmDD-XXXX.pdf" nevu pdf fajlokat hasznalja (hosszak fontosak, X->barmilyen karakter). A dátum részt értelmezi, és kiszámolja a hét szombati napját, ezt a tartományt megjeleníti a kép felett.

## Telepítés

1. Töltsd fel a plugin fájlokat a WordPress oldaladra a `wp-content/plugins/` könyvtárba.
2. Aktiváld a bővítményt a WordPress admin felületen a "Bővítmények" menüpontban.
3. Másold be a /public_html/etlap_pdf/ mappába a pdf fájlokat.

## Használat

A plugin használatához egyszerűen illeszd be a `[etlap_pdf_fajlok]` rövidkódot az oldalak vagy bejegyzések szerkesztőjébe. Alapértelmezetten a plugin az `etlap_pdf` nevű mappában keresi azokat amik a névformának megfelelő a PDF fájlok.

```shortcode
[etlap_pdf_fajlok]

```
```
cd existing_repo
git remote add origin https://gitlab.com/whriley/etlap-megjelenito-wordpress-plugin.git
git branch -M main
git push -uf origin main
```
## Project status
Demo: https://www.laposvendeglo.hu/heti-menu/
